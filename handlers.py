import json
from hashlib import sha256
from uuid import uuid4
from bson import ObjectId
from motor import MotorClient
from tornado import gen
from tornado.web import RequestHandler
from apns2.client import APNsClient
from apns2.payload import Payload, PayloadAlert
from apns2.credentials import TokenCredentials

json_old_serializer = json.JSONEncoder.default


def json_serializer(self, obj):
    if isinstance(obj, ObjectId):
        return str(obj)
    else:
        return json_old_serializer(self, obj)


json.JSONEncoder.default = json_serializer


def get_db():
    return MotorClient().pushdispatcher


def get_users():
    return get_db().users


class BaseHandler(RequestHandler):
    def not_authorized(self, *args, **kwargs):
        self.set_status(403, u'json error')

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)
        try:
            self.data = json.loads(self.request.body) if self.request.method == 'POST' else None
        except ValueError:
            self.get = self.post = self.not_authorized

    @property
    def db(self):
        return self.settings['db']

    @property
    def users(self):
        return self.db.users

    def error(self, reason: str) -> None:
        res = {
            'status': False,
            'data': None,
            'error': reason
        }
        self.write(res)

    def ok(self, data) -> None:
        res = {
            'status': True,
            'data': data,
            'error': None
        }
        self.write(res)


def get_hash(password):
    s = password + '3332aaa0-6bef-4092-b419-7106126472e8'
    r = sha256(s.encode('utf8')).hexdigest()
    return r


class RegisterHandler(BaseHandler):
    async def _check(self, email):
        user = await self.users.find_one({'email': email})
        return user is not None

    async def post(self):
        try:
            email = str(self.data['email'])
            error = await self._check(email)
            if error:
                self.error(u'Пользователь с таким email уже существует')
                return

            name = str(self.data['name'])
            passw = get_hash(str(self.data['password']))
            device_token = self.data.get('device_token', None)
            device_os = self.data['os']
            if device_token is not None:
                device_token = str(device_token)
            user = {
                "name": name,
                "email": email,
                "passw": passw,
                "device_token": device_token,
                "session": uuid4().hex,
                "os": device_os
            }
            await self.users.insert_one(user)
            self.set_cookie('session', user['session'])
            self.ok(user['session'])
        except KeyError as e:
            self.error(u'Не указан параметр %s' % e.args[0])


class AuthorizedHandler(BaseHandler):
    def auth_error(self, *args, **kwargs):
        self.error('auth_error')

    async def prepare(self):
        try:
            session = self.get_cookie('session')
            user = await self.users.find_one({'session': session})
            if user:
                if self.get_cookie('device_token', None) != user['device_token']:
                    raise KeyError
                self.current_user = user
            else:
                raise KeyError
        except KeyError:
            self.get = self.post = self.auth_error


class LoginHandler(BaseHandler):
    async def post(self):
        try:
            email, password = self.data['email'], self.data['password']
            token = self.data.get('device_token', None)
            user = await self.users.find_one({'email': email})
            if not user:
                self.error(u'Пользователь не найден')
                return
            if user['passw'] != get_hash(password):
                self.error(u'Неверные почта или пароль')
                return
            session = uuid4().hex
            device_os = self.data['os']
            await self.users.update_one({'_id': user['_id']}, {'$set': {
                'session': session,
                'device_token': token,
                'os': device_os
            }})
            self.set_cookie('session', session)
            if token:
                self.set_cookie('device_token', token)
            self.ok(session)
        except KeyError as e:
            self.error(u'Не указан параметр %s' % e.args[0])


class StartAppHandler(AuthorizedHandler):
    async def post(self):
        from asyncio import sleep
        await sleep(5)
        self.ok({})


class AllUsersHandler(BaseHandler):
    async def get(self):
        users = await self.users.find({}).to_list(None)
        self.ok(list(map(dict, users)))


class SendMessageHandler(BaseHandler):
    @gen.coroutine
    def _send_msg(self, client, token_hex, payload, topic):
        client.send_notification(token_hex, payload, topic)
        raise gen.Return()

    async def post(self):
        topic = 'ru.bitnic.triviat'
        msg = self.data['msg']
        title = self.data['title']
        auth_key_path = '/home/jut/AuthKey_3RK6Q6BQ49.p8'
        # auth_key_path = '/root/AuthKey_3RK6Q6BQ49.p8'
        auth_key_id = '3RK6Q6BQ49'
        team_id = '2XBZCWADKN'
        token_hex = self.data['to_device']
        alert = PayloadAlert(
            title=title,
            body=msg,
        )
        payload = Payload(alert=alert, sound="default", badge=0, url_args="https://go.bitnic.ru")
        token_credentials = TokenCredentials(auth_key_path=auth_key_path, auth_key_id=auth_key_id, team_id=team_id)
        client = APNsClient(credentials=token_credentials, use_sandbox=True)
        await self._send_msg(client, token_hex, payload, topic)
