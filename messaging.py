import asyncio
from apns2.client import APNsClient
from apns2.credentials import TokenCredentials
from apns2.payload import PayloadAlert, Payload
from tornado import gen


async def _send_to_ios(token_hex, title, msg):
    @gen.coroutine
    def _send_msg():
        r = client.send_notification_async(token_hex, payload, topic)
        raise gen.Return(client.get_notification_result(r))

    topic = 'ru.bitnic.triviat'
    auth_key_path = '/home/jut/AuthKey_3RK6Q6BQ49.p8'
    # auth_key_path = '/root/AuthKey_3RK6Q6BQ49.p8'
    auth_key_id = '3RK6Q6BQ49'
    team_id = '2XBZCWADKN'
    alert = PayloadAlert(
        title=title,
        body=msg,
    )
    payload = Payload(alert=alert, sound="default", badge=1, url_args="https://go.bitnic.ru")
    token_credentials = TokenCredentials(auth_key_path=auth_key_path, auth_key_id=auth_key_id, team_id=team_id)
    client = APNsClient(credentials=token_credentials, use_sandbox=True)
    return await _send_msg()


async def send_message(user, title, msg):
    if not user['device_token']:
        return 'У пользователя нет токена'

    if user['os'] == 'ios':
        try:
            r = await _send_to_ios(user['device_token'], title, msg)
            return r
        except BaseException as e:
            return e
    elif user['os'] == 'android':
        pass
    else:
        return f'Неизвестная ОС {user["os"]}'


async def jut():
    from handlers import get_users
    print('start')
    user = await get_users().find_one({'device_token': '6544aeebae98fb9d5521205a1349497f4645bee10d442a87e9173576f6503a7d'})
    result = await send_message(user, 'Тест функции', 'Тестовое сообщение')
    return result


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(asyncio.gather(jut()))
    print(result)
