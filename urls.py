from handlers import *


urls = (
    ('/register', RegisterHandler),
    ('/login', LoginHandler),
    ('/startapp', StartAppHandler),
    ('/allusers', AllUsersHandler),
    ('/send_msg', SendMessageHandler),
)
